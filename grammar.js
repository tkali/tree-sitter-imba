// @ts-nocheck
module.exports = grammar({
  name: "IMBA",
  rules: {
    source_file: ($) => repeat($._definition),

    _definition: ($) =>
      choice(
        $.tag
        // TODO: more definitions
      ),

    tag: ($) => seq($.tag_open, $._tag_content, $.tag_close),

    tag_open: () => "<",

    _tag_content: ($) => seq($.tag_identifier, repeat($._tag_inline_style)),

    _tag_inline_style: ($) =>
      seq(
        $.inline_style_open,
        repeat($.inline_style_prop),
        $.inline_style_close
      ),

    tag_close: () => ">",

    tag_identifier: () =>
      token(seq(/[a-z]+/, repeat(optional(seq("-", /[a-z]+/))))),

    // inline CSS
    inline_style_open: () => "[",
    inline_style_close: () => "]",
    inline_style_prop: () => token(seq(/[a-z,@]+/, /:\s*/, /[a-z,#,0-9]+/)),
  },
});
